import React, { Component } from "react";
import { Select } from 'antd';
import styles from "./style.module.css";
import { ProductsTable } from "../ProductsTable";
import { TitleSearch } from "../TitleSearch";
import { Input } from "antd";


import axios from 'axios';

class ProductsSection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            eventsData: [],
            departmentFilter: '',
            pagination: {
                current: 1,
                total: 0
            },
            product_name_filter: '',
            promotion_code: '',
            department_name: '',
            departments: []
        };
    }

    getQueryObjFromState = () =>{
        return {
            page_number: this.state.pagination.current,
            per_page: 10,
            product_name: this.state.product_name_filter,
            promotion_code: this.state.promotion_code,
            department_name: this.state.department_name
        }
    }

    componentDidMount(){
        axios.get('http://localhost:3000/departments')
            .then(response => this.setState({departments: response.data, pagination: {
                    current: 1,
                    total: 0
                }}))
            .then( _ => this.fetchDataAndReact(this.getQueryObjFromState()));
    }

    handleSearch = searchText => {
        this.setState({product_name_filter: searchText, pagination: {
                current: 1,
                total: 0
            }});
        return this.fetchDataAndReact({...this.getQueryObjFromState(), product_name: searchText, page_number: 1, per_page: 10})
    };

    handlePromotionSearch = promotion => {
        this.setState({promotion_code: promotion, pagination: {
            current: 1,
                total: 0
            }});
        return this.fetchDataAndReact({...this.getQueryObjFromState(), promotion_code: promotion, page_number: 1, per_page: 10})
    }

    handleDepartmentFilter = (value) => {
        this.setState({department_name: value, pagination: {
                current: 1,
                total: 0
            }});
        return this.fetchDataAndReact({...this.getQueryObjFromState(), page_number: 1, per_page: 10, department_name: value})
    }

    handlePaginate = (page, pageSize) => {
        // this.setState({pagination: {...this.state.pagination, current: page, }});
        return this.fetchDataAndReact({...this.getQueryObjFromState(), page_number: page, per_page: pageSize});
    }

    fetchDataAndReact = (searchQuery) => {
        Object.keys(searchQuery)
            .filter(key => searchQuery[key] == null || (typeof searchQuery[key] === 'string' && searchQuery[key].length === 0))
            .forEach(key => delete searchQuery[key]);
        return axios.get(
            `http://localhost:3000/products`,
            {
                params: searchQuery
            }
        ).then((response) => {
            if(response.status !== 200){
                throw new Error("Request to fetch products failed")
            }
            const {products, count, page_number, per_page} = response.data;
            this.setState({
                eventsData: products,
                pagination: {
                    current: Number.parseInt(page_number),
                    total: count
                }
            })
        });
    }


    render() {
        const Search = Input.Search;
        const { Option } = Select;
        return (
            <section className={styles.container}>
                <header className={styles.header}>
                    <h1 className={styles.title}>Events</h1>
                    {/*TODO refactor to seperate component*/}
                    <div className={styles.action}>
                        <Search
                            placeholder="Enter Promotion Code"
                            onSearch={this.handlePromotionSearch}
                            allowClear={true}
                            style={{ width: 200 }}
                        />
                    </div>
                    <Select
                        className={styles.department_dropdown}
                        style={{ width: 120 }}
                        onChange={this.handleDepartmentFilter}
                        allowClear={true}
                        placeholder="Department"
                    >
                        {this.state.departments.map((department, idx) => <Option key={idx} value={department.name}>{department.name}</Option>)}
                    </Select>
                    <TitleSearch onSearch={this.handleSearch} className={styles.action} />
                </header>
                <ProductsTable eventsData={this.state.eventsData} pagination={{...this.state.pagination, onChange: this.handlePaginate}}/>
            </section>
        );
    }
}

export { ProductsSection };
