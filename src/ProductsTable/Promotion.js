import React  from "react";
import {Tag} from "antd";
export default function Promotion(props){
    let promotion = props.promotion;
    promotion = promotion == null ? {} : promotion;
    const color = promotion.active ? "green" : "red";
    return <Tag color={color}>
        {promotion.code} - {promotion.discount}%
    </Tag>;
}
