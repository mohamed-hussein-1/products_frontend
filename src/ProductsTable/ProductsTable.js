import React from "react";
import Table from "antd/lib/table";
import "antd/lib/table/style/css";
import "antd/lib/button/style/css";
import Promotions from "./Promotions";

const ProductsTable = ({ eventsData, pagination }) => {
    const tableColumns = [
        {
            title: "ID",
            dataIndex: "id",
            key: "id"
        },
        {
            title: "Name",
            dataIndex: "name",
            key: "name"
        },
        {
            title: "Department",
            dataIndex: "department.name"
        },
        {
            title: "price",
            dataIndex: "price",
            key: "price"
        },
        {
            title: "lowest price with promotion",
            key: "lowest_promotion",
            render: (text, record) => {
                const prom = record.promotions;
                let min_promotion = prom
                    .filter(promotion => promotion.active)
                    .map(promotion => promotion.discount)
                    .reduce(((prev, curr) => Math.max(prev, curr)), 0)
                ;
                min_promotion = min_promotion == null ? 0 : min_promotion;
                return (record.price - min_promotion * (record.price) / 100.0).toFixed(2);
            }
        },
        {
            title: "promotions",
            key: "promotions",
            render: (text, record) => {
                return <Promotions promotions={record.promotions}/>;
                // return (record.promotions || [])
                //     .sort((a, b) => a.discount - b.discount)
                //     .reduce((acc, memo) => {return acc + `(${memo.code}) : ${memo.active ? "active" : "disabled"} : ${memo.discount}% - `;},"")
            }
        }
    ];


    return <Table dataSource={eventsData}
                  columns={tableColumns}
                  pagination={{
                      // pageSizeOptions : ['10'],
                      // showSizeChanger : true,
                      ...pagination
                      // current: pagination.pageNumber,
                      // total: pagination.count,
                      // pageSize: 1,
                  }}/>;
};

export { ProductsTable };
