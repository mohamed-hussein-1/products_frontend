import React, { Component } from "react";
import Promotion from "./Promotion";

class Promotions extends Component{
    render(){
        let {promotions} = this.props;
        promotions = (promotions || []);
        return <React.Fragment>
            {promotions
                .sort((a, b) => parseFloat(a.discount) - parseFloat(b.discount))
                .map((promotion) => <Promotion promotion={promotion}/>)}
        </React.Fragment>

    }
}

export default Promotions;
