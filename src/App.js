import React from 'react';
import { Form, Select, InputNumber, DatePicker, Switch, Slider, Button } from 'antd';
import './App.css';
import {ProductsSection} from "./ProductsSection/ProductsSection";


const { Option } = Select;

const App = () => (
  <div className="App">
    <ProductsSection />
  </div>
);

export default App;
