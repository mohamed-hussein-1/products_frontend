# 
## Preview

- ensure you have the backend working on `localhost:3000`
- run these commands
```bash
$ npm install
$ npm start
```

or:

```bash
$ yarn
$ yarn start
```

